const ipc = require('electron').ipcRenderer;
const forge = require('node-forge');
const fs = require('fs')
const path = require('path')

ipc.on('decrypt', function(event, password, prefix, pathname, pathindex) {
  event.sender.send('start_decrypt', pathindex);
  var key = password + Array(24).join(' ')
  key = key.slice(0,24)
  var cipher = forge.cipher.createDecipher('DES-ECB', key);
  pathlist = pathname.split(path.sep)
  filename = pathlist.pop()
  var outname = pathlist.concat(prefix+filename).join(path.sep)
  fs.readFile(pathname, {encoding: 'binary'}, (err, input) => {
    if (err) throw err;
    cipher.start();
    cipher.update(forge.util.createBuffer(input, 'binary'))
    cipher.finish()
    var output = forge.util.createBuffer()
    output.putBuffer(cipher.output)
    fs.writeFileSync(outname, output.getBytes(), {encoding: 'binary'})
    event.sender.send('end_decrypt', pathindex);
  })
});
