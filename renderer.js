const ipc = require('electron').ipcRenderer;

const dropzone = document.getElementById("dropzone");

dropzone.ondragleave = dropzone.ondragend = dropzone.ondragover = () => {
  return false;
}

dropzone.ondrop = function (event) {
  event.preventDefault()
  for (let f of event.dataTransfer.files) {
    item = document.createElement('div');
    item.className = "item";
    icon = document.createElement('i');
    icon.className = "notched circle icon";
    content = document.createElement('i');
    content.className = "content";
    txt = document.createTextNode(f.path);
    content.appendChild(txt);
    item.appendChild(icon);
    item.appendChild(content);
    content.appendChild(txt);
    document.getElementById('droplist').appendChild(item);
  }
  return false;
};

ipc.on('start_decrypt', function(event, fileindex) {
    item = document.getElementById('droplist').children[fileindex];
    item.children[0].className = 'notched circle loading icon';
});

ipc.on('end_decrypt', function(event, fileindex) {
    item = document.getElementById('droplist').children[fileindex];
    item.children[0].className = 'checkmark icon';
});

const decryptbutton = document.getElementById('button_decrypt');

decryptbutton.addEventListener('click', function () {
  const prefix = "e_";
  const password = document.getElementById('password').value;
  files = document.getElementById('droplist').children;
  Array.from(files).forEach(function(node, index) {
    ipc.send('decrypt', password, prefix, node.children[1].innerHTML, index);
  });
});
