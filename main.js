const electron = require('electron');
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;
const forge = require('node-forge');
const ipc = require('electron').ipcMain;
const fs = require('fs');
const path = require('path');
const url = require('url');


let mainWindow;

function createWindow () {
  if (process.env.DEBUG==1) {
    require('electron-reload')(__dirname);
    mainWindow = new BrowserWindow({
      width: 1500,
      height: 380,
      resizable: true,
    });
    mainWindow.webContents.openDevTools()

    workerWindow = new BrowserWindow({title: 'worker', width:1200, height:400});
    workerWindow.webContents.openDevTools()
  }
  else
  {
    mainWindow = new BrowserWindow({
      width: 800,
      height: 380,
      resizable: true,
      titleBarStyle: 'default'
    });
    workerWindow = new BrowserWindow({show:false});
  }

  mainWindow.loadURL(url.format({
    pathname: path.join(__dirname, 'index.html'),
    protocol: 'file:',
    slashes: true
  }));

  workerWindow.loadURL(url.format({
    pathname: path.join(__dirname, 'worker.html'),
    protocol: 'file:',
    slashes: true
  }));

  mainWindow.webContents.on('will-navigate', event => {
    event.preventDefault()
  });

  mainWindow.on('closed', function () {
    mainWindow = null
  });

  // can't do decryption in main or mainWindow's renderer with smooth DOM updates
  // so relay decrypt requests from mainWindow to workerWindow (they can't talk directly)
  ipc.on('decrypt', function(event, password, prefix, pathname, pathindex) {
    workerWindow.webContents.send('decrypt', password, prefix, pathname, pathindex);
  });

  // receive workers' update and relay back to mainWindow
  ipc.on('start_decrypt', function(event, pathindex) {
    mainWindow.webContents.send('start_decrypt', pathindex);
  });

  ipc.on('end_decrypt', function(event, pathindex) {
    mainWindow.webContents.send('end_decrypt', pathindex);
  });
};

app.on('ready', createWindow);
app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', function () {
  if (mainWindow === null) {
    createWindow();
  }
});
